<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="student_database.admin"%>
<%@page import="student_database.admin_methods"%>
<!DOCTYPE html>
<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
    
%>
<html >
    
<head>
  <meta charset="UTF-8">
  <title>search student</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  

      <link rel="stylesheet" href="css/style.css">

  
</head>
<style>
        table, th, td 
        {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td 
        {
            padding: 5px;
            text-align: left;
        }
        table#t01 
        {
            width: 100%;    
            background-color: #f1f1c5;
        }
        body
               {
                        background:url(images/1.jpg);
                        background-size:cover;
                        margin:0;
                }
        .blinking {
	-webkit-animation: blink .75s linear infinite;
	-moz-animation: blink .75s linear infinite;
	-ms-animation: blink .75s linear infinite;
	-o-animation: blink .75s linear infinite;
	 animation: blink .75s linear infinite;
}
@-webkit-keyframes blink {
	0% { opacity: 1; }
	50% { opacity: 1; }
	50.01% { opacity: 0; }
	100% { opacity: 0; }
}
@-moz-keyframes blink {
	0% { opacity: 1; }
	50% { opacity: 1; }
	50.01% { opacity: 0; }
	100% { opacity: 0; }
}
@-ms-keyframes blink {
	0% { opacity: 1; }
	50% { opacity: 1; }
	50.01% { opacity: 0; }
	100% { opacity: 0; }
}
@-o-keyframes blink {
	0% { opacity: 1; }
	50% { opacity: 1; }
	50.01% { opacity: 0; }
	100% { opacity: 0; }
}
@keyframes blink {
	0% { opacity: 1; }
	50% { opacity: 1; }
	50.01% { opacity: 0; }
	100% { opacity: 0; }
}
        
        
</style>
<body>
  
  
        
  <div align="center">
  <br>
  <br>
  <br>
  <div class="text">enter student details</div>
  <br>
  <br>
  <br>
  <form action="search_suggestions" method="POST">
        <input class="button" placeholder="Search..." name="name"/><br><br>
        <input type="submit" value="Submit">
       
  
  </form>
  

</body>

<table bgcolor="white">
    
        
        <%
            session=request.getSession();
            String search=(String)session.getAttribute("search");
            session.setAttribute("ID", search);
            if(search==null)
            {
                %>
                <span class="blinking"><h4><font color="red">Please search any id to show results...</font></h4></span>
                <%
            }
            else
            { 
              %>
              
              <%
                 session.setAttribute("search", null);
                admin_methods m=new admin_methods();

                List <admin> list=(List)m.viewrequests(search);
                if(!list.isEmpty())
                {%>
                    <thead>
                        <tr>

                                <th>id</th><br>
                                <th>name</th><br>
                                <th>branch</th><br>
                                <th>year</th><br>
                                <th>mobile</th><br>
                                <th>click</th>

                        </tr>
                    </thead>  
                <%}
                else
                {
                session.setAttribute("search", null);
                %>
                     <span class="blinking"><h4><font color="red">No such records found...</font></h4></span>
                <%}
                Iterator<admin> itr=list.iterator();
                for(admin c:list)
                {%>
                
                <tr>
                    <td><%=c.gets_id()%></td>
                    <td><%=c.gets_name()%></td>
                    <td><%=c.gets_branch()%></td>
                    <td><%=c.gets_year()%></td>
                    <td><%=c.gets_mobile()%></td>
                    <TD>
                        <form action="student_profile" method="post">
                            <input type="hidden" name="ID" value="<%=c.gets_id()%>"/>
                            
                            <input type="image" src="images/play.svg"  alt="submit" width="70" height="50"/>                  
                        </form>
                    </TD>
                    

                </tr>
               
               <%
                   
            }}
        %>
           
</table>
</html>
