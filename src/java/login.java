import student_database.admin_methods;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.sql.*;

public class login extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
           
            String a_username=request.getParameter("username");
            String a_password=request.getParameter("password");
            
             
           
        response.setContentType("text/html;charset=UTF-8");
        
                
                PrintWriter out=response.getWriter();
           
                                
                                admin_methods adm=new admin_methods();
                                int flag=adm.login(a_username,a_password);
                                HttpSession session=request.getSession();
                                if(flag==1)
                                {
                                        
                                        
                                        session.setAttribute("username",a_username);
                                        response.sendRedirect("search.jsp");
                                }
                                else
                                {
                                        out.println("incorrect details entered.");
                                        response.sendRedirect("index.jsp");
                                    
                                }
                        }
                        
        
    @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
        {
                processRequest(request,response);
        }
}
                                

