package student_database;
public class e3_sem1 
{
     private String id;
     private String subjects;
     private int credits;
     private float mid1;
     private float mid2;
     private float mid3;
     private String grade;
     private float cgpa;
     private float sgpa;

    public e3_sem1() 
    {
        
    }

    e3_sem1(String id,String subjects,int credits,float mid1,float mid2,float mid3,String grade)
    {
    }

    

    public String getid() {
        return id;
    }

    public String getsubjects() {
        return subjects;
    }

    public int getcredits() {
        return credits;
    }

    public float getmid1() {
        return mid1;
    }

    public float getmid2() {
        return mid2;
    }

    public float getmid3() {
        return mid3;
    }

    public String getgrade() {
        return grade;
    }

    public float getcgpa() {
        return cgpa;
    }

    public float getsgpa() {
        return sgpa;
    }

    public void setid(String id) {
        this.id = id;
    }

    public void setsubjects(String subjects) {
        this.subjects = subjects;
    }

    public void setcredits(int credits) {
        this.credits = credits;
    }

    public void setmid1(float mid1) {
        this.mid1 = mid1;
    }

    public void setmid2(float mid2) {
        this.mid2 = mid2;
    }

    public void setmid3(float mid3) {
        this.mid3 = mid3;
    }

    public void setgrade(String grade) {
        this.grade = grade;
    }

    public void setcgpa(float cgpa) {
        this.cgpa = cgpa;
    }

    public void setsgpa(float sgpa) {
        this.sgpa = sgpa;
    }

    @Override
    public String toString() {
        return "e3_sem1{" + "id=" + id + ", subjects=" + subjects + ", credits=" + credits + ", mid1=" + mid1 + ", mid2=" + mid2 + ", mid3=" + mid3 + ", grade=" + grade + '}';
    }
     
     
     
     
    
}
